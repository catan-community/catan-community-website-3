import datetime
import os
import urllib.parse
from collections import namedtuple

import flask
import flask_migrate
import redis
import simple_flask_cms
from flask import Flask
from flask_session import Session

import blueprints.auth as auth
import blueprints.invites as invites
import blueprints.profile as profile
import blueprints.registration as registration
import blueprints.stats as stats
import blueprints.tournaments as tournaments
import util.global_cache as global_cache
import util.leaderboard_util as leaderboard_util

app = Flask(__name__)
csrf = global_cache.csrf
csrf.init_app(app)
app.register_blueprint(simple_flask_cms.cms)
csrf.exempt(simple_flask_cms.cms)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

global_cache.migrate = flask_migrate.Migrate(app, global_cache.sql_db)

simple_flask_cms.config.db_connection = (
    simple_flask_cms.database_providers.sql_database.SQLDatabaseProvider(app)
)
simple_flask_cms.config.template_name = "page.html"
simple_flask_cms.config.fragments = [
    simple_flask_cms.dataclasses.FragmentType(
        name="home_tagline", description="tagline on the home page", url="/"
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_top_section",
        description="Part of the homepage above the 2 columns",
        url="/'",
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_left_column", description="Left Column of the homepage", url="/"
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_right_column", description="Right Column of the homepage", url="/"
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_bottom_area",
        description="The part of the homepage below the 2 call to action buttons",
        url="/",
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_quote_1",
        description="The part of the homepage below the 2 call to action buttons",
        url="/",
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_quote_2",
        description="The part of the homepage below the 2 call to action buttons",
        url="/",
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_quote_3",
        description="The part of the homepage below the 2 call to action buttons",
        url="/",
    ),
    simple_flask_cms.dataclasses.FragmentType(
        name="home_quote_4",
        description="The part of the homepage below the 2 call to action buttons",
        url="/",
    ),
]
simple_flask_cms.config.extra_nav_urls = [
    simple_flask_cms.dataclasses.ExtraNavUrl(
        path="home", redirect="/", nav_title="Home", sort_order=-999
    ),
    simple_flask_cms.dataclasses.ExtraNavUrl(
        path="leaderboard",
        redirect="/leaderboard",
        nav_title="Leaderboard",
    ),
    simple_flask_cms.dataclasses.ExtraNavUrl(
        path="leaderboard/month",
        redirect="/leaderboard/month",
        nav_title="month",
    ),
    simple_flask_cms.dataclasses.ExtraNavUrl(
        path="leaderboard/season",
        redirect="/leaderboard/season",
        nav_title="season",
    ),
]


def authentication_function(action, parameters):
    print("Authentication function called")
    if action in simple_flask_cms.config.viewer_paths:
        print("Accepted: Viewer path")
        return None
    if "AUTHENTICATION_KEY" in app.config and flask.request.headers.get(
        "X-authorization"
    ) == app.config.get("AUTHENTICATION_KEY"):
        return None
    if is_admin():
        return None
    return flask.render_template("403.html"), 403


simple_flask_cms.config.authentication_function = authentication_function

app.config.from_json(
    "config.json",
)

app.register_blueprint(registration.reg)
app.register_blueprint(stats.stats)
app.register_blueprint(auth.auth)
app.register_blueprint(profile.profile)
app.register_blueprint(invites.invite_manager)
app.register_blueprint(tournaments.tournaments)
app.before_request(invites.register_invite)

if app.config.get("SESSION_REDIS") is None:
    global_cache.redis_db = redis.Redis(
        app.config["REDIS"]["host"],
        port=app.config["REDIS"]["port"],
        password=app.config["REDIS"]["password"],
        db=0,
    )
    app.config["SESSION_REDIS"] = global_cache.redis_db
    app.config["CACHE_REDIS_HOST"] = app.config["SESSION_REDIS"]
    app.config["CACHE_REDIS_PORT"] = app.config["REDIS"]["port"]
    app.config["CACHE_REDIS_PASSWORD"] = app.config["REDIS"]["password"]

global_cache.cache.init_app(app)
Session(app)


def get_mongodb_connection():
    import pymongo

    client = pymongo.MongoClient(
        host=f'mongodb+srv://{urllib.parse.quote_plus(app.config["MONGODB"]["user"])}:'
        f'{urllib.parse.quote_plus(app.config["MONGODB"]["password"])}@{app.config["MONGODB"]["host"]}/'
        f'{app.config["MONGODB"]["database"]}?retryWrites=true&w=majority'
    )
    db = client.get_database(app.config["MONGODB"]["database"])
    print(db.users.count_documents({}))
    return db


def get_mongodb_database():
    if global_cache.mongodb is None:
        print("Creating NEW mongodb connection")
        global_cache.mongodb = get_mongodb_connection()
    else:
        print("re-using OLD mongodb connection")
    return global_cache.mongodb


@global_cache.cache.memoize(timeout=120)
def _is_admin(user_id):
    return (
        get_mongodb_database()["users"].find_one({"discord_username": f"<@{user_id}>"})
        or {}
    ).get("is_admin")


@app.template_global()
def is_admin():
    if flask.session.get("user_info") is not None:
        user_id_admin = _is_admin(flask.session["user_info"]["id"])

        if user_id_admin:
            print("Accepted, is admin")
            return True
        if (
            flask.request.remote_addr == "127.0.0.1"
            and os.environ.get("FLASK_ENV") == "development"
        ):
            print("Accepted, is localhost")
            return True
        print("Rejected: Not logged in")
    return False


@app.template_global()
def get_discord_invite():
    return "https://discord.gg/" + flask.session.get("discord_invite", "")


@app.template_global()
def get_year():
    return datetime.datetime.utcnow().year


@app.route("/")
def index():  # put application's code here
    return flask.Response(
        flask.render_template("home.html"),
    )


@app.route("/403")
def permission_denied():
    return flask.Response(flask.render_template("403.html"), status=403)


@app.route("/leaderboard")
@app.route("/leaderboard/<string:period>")
def leaderboard(period=None):
    if not period:
        return flask.redirect(flask.url_for("leaderboard", period="all"))
    normalized_period = leaderboard_util.normalize_period(period)
    if normalized_period != period:
        return flask.redirect(
            flask.url_for("leaderboard", period=normalized_period, **flask.request.args)
        )

    Page = namedtuple("Page", ("name", "url"))

    lp_filter = leaderboard_util.get_filter(
        period, league=flask.request.args.get("league")
    )
    children = [
        Page(
            url=flask.url_for("leaderboard", period=code, **flask.request.args),
            name=name,
        )
        for code, name in leaderboard_util.get_sub_periods(period or "all")
    ]

    leaderboard = leaderboard_util.get_leaderboard(
        get_mongodb_database(), lp_filter=lp_filter
    )
    status_code = 200 if leaderboard else 404

    return (
        flask.render_template(
            "leaderboard.html",
            leaderboard=leaderboard,
            children=children,
            league=leaderboard_util.format_league(flask.request.args.get("league")),
            date_formatted=leaderboard_util.format_filter(period),
        ),
        status_code,
    )


@app.route("/tournament")
def tournament():
    return flask.render_template("tournament_signup.html")


@app.before_first_request
def ensure_db():
    get_mongodb_database()


@app.before_request
def find_referer():
    if flask.session.get("referer") is None:
        flask.session["referer"] = flask.request.referrer


@app.template_filter()
def pairs(collection):
    for i in range((len(collection) + 1) // 2):
        if i + len(collection) // 2 >= len(collection):
            yield collection[i], None
        else:
            yield collection[i], collection[i + len(collection) // 2]


if __name__ == "__main__":
    app.run()

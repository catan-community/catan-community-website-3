# Setting up the repo

Find the website here: <https://catancommunity.org>

This was written by mousetail using ABH's tears.

1. Install python3.9+ and poetry.
2. Clone the repo
3. Create a discord application
4. Create a file `config.json` that looks like this:

```json
{
  "OAUTH2": {
    "client_id": "Get your client id From dicsord",
    "client_secret": "Get your client id from discord",
    "scope": [
      "identify"
    ]
  },
  "SECRET_KEY": "generate a secret key here",
  "MONGODB": {
    "host": "ccdevcluster0.ndot3.mongodb.net",
    "user": "database username",
    "password": "database password",
    "database": "CCDevDB0"
  },
  "SESSION_TYPE": "redis",
  "REDIS": {
    "password": "redis password",
    "host": "Redis host",
    "port": 17762
  }
}
```

5. Create a folder `media` with a subfolder `cms`
6. Run `poetry install`
7. Run the app using `poetry run app.py --debug`. If you get unhelpful error messages disable debug.
8. In discord, add a redirect url `http://locahost:5000/auth/oauth_callback` to your application. Note: http not https.

Is the homepage looking a bit blanc? Try filling in the fragments at `localhost:5000/cms/editor`
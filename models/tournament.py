import flask
from sqlalchemy.sql import func

from util import global_cache


class Tournament(global_cache.sql_db.Model):
    tournament_id = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, primary_key=True, nullable=False
    )
    short_name = global_cache.sql_db.Column(
        global_cache.sql_db.String(255), nullable=False
    )
    long_name = global_cache.sql_db.Column(
        global_cache.sql_db.String(255), nullable=False
    )
    description = global_cache.sql_db.Column(
        global_cache.sql_db.String(255), nullable=False
    )
    role_id = global_cache.sql_db.Column(global_cache.sql_db.Integer, nullable=True)
    game_mode = global_cache.sql_db.Column(
        global_cache.sql_db.String(32), nullable=True
    )
    vp_to_win = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, nullable=False, default=10
    )

    players_per_team = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, nullable=False, default=1
    )
    teams_per_table = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, nullable=False, default=4
    )


class TournamentPlayer(global_cache.sql_db.Model):
    id = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, primary_key=True, nullable=False
    )
    tournament_id = global_cache.sql_db.Column(
        global_cache.sql_db.Integer,
        global_cache.sql_db.ForeignKey(Tournament.tournament_id),
    )
    discord_id = global_cache.sql_db.Column(global_cache.sql_db.Integer)
    discord_username = global_cache.sql_db.Column(
        global_cache.sql_db.String(128), nullable=False
    )
    colonist_username = global_cache.sql_db.Column(
        global_cache.sql_db.String(128), nullable=False
    )

    round_number = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, nullable=False
    )

    table_number = global_cache.sql_db.Column(
        global_cache.sql_db.Integer, nullable=False
    )

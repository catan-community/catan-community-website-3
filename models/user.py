import flask
from sqlalchemy.sql import func

import blueprints.registration
from util import global_cache


class User(global_cache.sql_db.Model):
    user_id = global_cache.sql_db.Column(global_cache.sql_db.Integer, primary_key=True)
    username = global_cache.sql_db.Column(
        global_cache.sql_db.String(80), unique=True, nullable=False
    )
    colonist = global_cache.sql_db.Column(
        global_cache.sql_db.String(80), unique=True, nullable=False
    )
    country = global_cache.sql_db.Column(global_cache.sql_db.String(80))
    email = global_cache.sql_db.Column(
        global_cache.sql_db.String(120), unique=True, nullable=True
    )
    use_discord = global_cache.sql_db.Column(global_cache.sql_db.String(15))
    assistance = global_cache.sql_db.Column(global_cache.sql_db.String(500))
    ck = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    ck_5_6 = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    sf = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    sf_5_6 = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    cksf = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    cksf_5_6 = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    base_5_6 = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    base_7_8 = global_cache.sql_db.Column(
        global_cache.sql_db.Boolean, nullable=False, default=False
    )
    referer = global_cache.sql_db.Column(
        global_cache.sql_db.String(127), nullable=False, default=""
    )
    registration_date = global_cache.sql_db.Column(
        global_cache.sql_db.DateTime, default=func.now()
    )
    registration_update_date = global_cache.sql_db.Column(
        global_cache.sql_db.DateTime, default=func.now(), onupdate=func.now()
    )

    def update_auxiliary_fields(self, form: "blueprints.registation.RegistrationForm"):
        self.colonist = form.colonist.data
        self.country = form.country.data
        self.email = form.email.data or None
        self.use_discord = form.use_discord.data
        self.assistance = form.assistance.data
        self.ck = form.ck.data
        self.ck_5_6 = form.ck_5_6.data
        self.sf = form.sf.data
        self.sf_5_6 = form.sf_5_6.data
        self.cksf = form.cksf.data
        self.cksf_5_6 = form.cksf_5_6.data
        self.base_5_6 = form.base_5_6.data
        self.base_7_8 = form.base_7_8.data
        if not self.referer:
            self.referer = flask.session.get("referer", "")
        global_cache.sql_db.session.commit()

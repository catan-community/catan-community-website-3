git fetch origin --tags
git switch --detach $BRANCH
. venv/bin/activate
poetry install
npm install
npm run prod
flask db upgrade
sudo systemctl restart catan-community-website-3.service
deactivate
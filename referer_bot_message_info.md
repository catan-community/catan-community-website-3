So you got a message that looks like this:

```txt
User umikandre#5588 joined using a invite made by HippoBot-O-Mus#4120.Recently, someone visited the website got shown this invite. They may or may not be the same person:
Referrer: 'https://colonist.io/'
First page visited: 'https://catancommunity.org/cms/Tournaments/signup'
Last page visited: 'https://catancommunity.org/registration/register/553711563358339100'
First seen: '2022-07-16T22:30:57.507406'
Last time: '2022-07-16T22:31:33.163523'
If the last time is a long time ago the chance that this is the same person is less
```

What does it mean?

# What is the purpose of the system.

When looking at [the analytics page](https://counter.dev/dashboard.html?user=mousetail&token=Ba3z9HV%2BBvFqn%2B8x) you can see a lot of visitors from different sites. However,
It's hard to tell how many people really engage with the site from each traffic source. Seems likely that people that join via youtube might be less likely to be willing to
join the discord than people from other sites.

With this reason I made the [new users page](https://catancommunity.org/stats/new-users) that shows the referer so you know how many people registered. Data there seems
pretty accurate but many people join our server and never register. What's up with that?

I wanted some way to link the visitors of our site to the visitors of the website, so we know where they come from, what pages they usually see first, etc. We can cross reference this with [the analytics page](https://counter.dev/dashboard.html?user=mousetail&token=Ba3z9HV%2BBvFqn%2B8x) to see how many people actually join the server. We can use this to
better aim our promotion efforts.

The system is *not* designed to catch bots or alt accounts.

# How does it work.

I generated around 300 invites using Alarm Bot. The website will show each visitor a random invite. For each invite generated the server will store some information about the user, like their referer and the first and last page they visited.

When the bot sees someone joined the server, it will check with the website if someone recently got shown the same invite. Of course, depending on the traffic the "most recent person to see the invite" might not be the same as the person joining the server. However, considering the significant number of invites the chance is high.

# If the last visited page is something like season 9999 are they most likely a bot?

No. There are a ton of bots crawling the website and likely to end up on season 9999. These are not the same as the bots that join discord servers. Most likely a bot visited the website and happened to get the same invite in between the visit and the join.

# If the referer is not listed, is the user most likely a bot?

No. There are many reasons the referer could not be available, including:

* Visitng the webiste directly without using a search engine or clicking a link
* The link they clicked to visit the website has the "noreferrer" atribute set that prevents browsers from sending the referrer header.
* The user has disabled sending the header in their browser settings. Many privacy focused plugins do this.

# If the first and last seen time are the same, is the user most likely a bot?

No. I only record times when people visit a new page. So if you visit only one page the times will always be the same. Again bots focused on messing up discords avoid supsicious behavior on websites. The type of bots that attack or crawl websites don't have the ability to join discord servers.

# The referer on the registation form does not match the referer on the join message, is this person a bot?

No. Probably just means they closed and re-opened the tab in between.

# The "last visited time" is a long time ago, is this person a bot?

No, most likely they either got sent a link by another player or just saved it for a bit and joined later. No need to worry. However, the chance that the website visitor and
discord joined become a lot lower. Many people could have been shown the same link in that time.

# The referer is a webpage inside the website itself, what does this mean?

It means the referer was not set when the person first visited the webiste. So it will be set to the first page on the next page they visit.

# A user is suspicous based on their referer or other info, should we ban them?

*No*, the data is not accurate enough on the small scale. Don't make any decisions about a specific person based on how they joined. Decisions can be made based on the *number* of people that join via
a referer on average. Not on specific people.
import datetime

import util.global_cache as global_cache

months = (
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
)
assert len(months) == 12


def is_month_period(period):
    return period.count("-") == 1 and period.split("-")[0] in months


def format_month_filter(month, year):
    return months[month].capitalize() + ", " + str(year)


def format_season_filter(season_number):
    return f"Season {season_number}"


def get_sub_periods(period: str):
    if not isinstance(period, str):
        raise TypeError("Period must be string")
    if is_month_period(period):
        month_string, year_string = period.split("-")
        year = int(year_string)
        month = months.index(month_string)

        if month == 0:
            yield months[-1] + "-" + str(year - 1), format_month_filter(11, year - 1)
        else:
            yield months[month - 1] + "-" + str(year), format_month_filter(
                month - 1, year
            )

        season_number = month // 3 + (year - 2021) * 4 + 2
        yield "s" + str(season_number), format_season_filter(season_number)

        if month == 11:
            yield months[0] + "-" + str(year + 1), format_month_filter(0, year + 1)
        else:
            yield months[month + 1] + "-" + str(year), format_month_filter(
                month + 1, year
            )

    elif period[0] == "s":
        season_number = int(period[1:])

        if season_number >= 3:
            yield f"s{season_number - 1}", format_season_filter(season_number - 1)

        year = (season_number - 2) // 4 + 2021
        yield str(year), str(year)

        for month in range(
            ((season_number - 2) % 4) * 3, ((season_number - 2) % 4) * 3 + 3
        ):
            yield f"{months[month]}-{year}", format_month_filter(month, year)

        yield f"s{season_number + 1}", format_season_filter(season_number + 1)

    elif period.isnumeric():
        year = int(period)
        yield str(year - 1), str(year - 1)

        yield "all", "All Time"

        season_number = (year - 2021) * 4 + 2
        for i in range(4):
            yield f"s{season_number + i}", format_season_filter(season_number + i)
        yield str(year + 1), str(year + 1)
    elif period == "all":
        for i in range(2021, datetime.datetime.now().year + 1):
            yield str(i), str(i)


def normalize_period(period):
    time = datetime.datetime.now(datetime.timezone.utc)
    if period == "month":
        period = f"{months[time.month - 1]}-{time.year}"
    elif period == "season":
        season = (time.month - 1) // 3 + (time.year - 2021) * 4 + 2
        period = f"s{season}"
    return period


def get_filter(
    period: str = None, mode: int = None, nrof_players: int = None, league=None
):
    period = normalize_period(period)

    lp_filter = {}
    if period and period != "all":
        if is_month_period(period):
            month_string, year_string = period.split("-")
            year = int(year_string)
            month = months.index(month_string)

            lp_filter["year"] = year
            lp_filter["month"] = month + 1
        elif period[0] == "s":
            season_number = int(period[1:])
            year = (season_number - 2) // 4 + 2021
            month = ((season_number - 2) % 4) * 3
            lp_filter["year"] = year
            lp_filter["month"] = {"$gte": month + 1, "$lte": month + 3}
        elif period.isnumeric():
            year = int(period)
            if year < 1900 or year > 10000:
                raise ValueError("Invalid Year")
            lp_filter["year"] = year
        else:
            raise ValueError("Invalid Period: ", period)

    if league is not None:
        if league == "ck":
            lp_filter["mode"] = {"$in": ["c&k", "c&k+seafarers"]}
        elif league == "base":
            lp_filter["mode"] = {"$in": ["base", "seafarers"]}
        else:
            raise ValueError(f"invalid league: {league!r}")
    return lp_filter


@global_cache.cache.memoize(args_to_ignore=["database"])
def get_user_rank(database, user_hash, lp_filter):
    lp = next(
        database["user_lp"].aggregate(
            [
                {"$match": {"user": user_hash, **lp_filter}},
                {
                    "$group": {
                        "_id": {"user": "$user"},
                        "lp": {"$sum": "$lp"},
                        "matches": {"$sum": "$matches"},
                        "normalized_wins": {"$sum": "$normalized_wins"},
                    }
                },
            ]
        ),
        {"lp": 0, "matches": 0, "normalized_wins": 0},
    )

    rank = (
        next(
            database["user_lp"].aggregate(
                [
                    {"$match": lp_filter},
                    {
                        "$group": {
                            "_id": {"user": "$user"},
                            "lp": {"$sum": "$lp"},
                        }
                    },
                    {"$match": {"lp": {"$gt": lp["lp"]}}},
                    {"$count": "count"},
                ]
            ),
            {"count": 0},
        )["count"]
        + 1
    )

    return {**lp, "rank": rank}


@global_cache.cache.memoize(args_to_ignore=["database"])
def get_leaderboard(database, start=0, end=50, lp_filter=None):
    return list(_get_leaderboard(database, start, end, lp_filter))


def _get_leaderboard(database, start=0, end=50, lp_filter=None):
    if lp_filter is None:
        lp_filter = {}
    users = database["user_lp"].aggregate(
        [
            {"$match": lp_filter},
            {
                "$group": {
                    "_id": {"user": "$user"},
                    "lp": {"$sum": "$lp"},
                    "matches": {"$sum": "$matches"},
                    "normalized_wins": {"$sum": "$normalized_wins"},
                }
            },
            {
                "$lookup": {
                    "from": "users",
                    "localField": "_id.user",
                    "foreignField": "_id",
                    "as": "user_data",
                }
            },
            {
                "$project": {
                    "lp": 1,
                    "matches": 1,
                    "normalized_wins": 1,
                    "user_data.display_name": 1,
                    "user_data.profile_picture_url": 1,
                    "user_data.discord_username": 1,
                }
            },
            {"$sort": {"lp": -1, "user_data.display_name": 1, "matches": -1}},
            {"$skip": start},
            {"$limit": end - start},
        ]
    )
    last_lp = 99999999
    last_rank = start

    for index, user in enumerate(users):
        user["player_id"] = int(user["user_data"][0]["discord_username"][2:-1])
        if last_lp == user.get("lp", 0):
            yield last_rank, user
        else:
            last_lp = user.get("lp", 0)
            last_rank = index + 1
            yield last_rank, user
            if last_rank + 1 >= end:
                break


def format_filter(period):
    if is_month_period(period):
        month, year = period.split("-")
        month = months.index(month)
        year = int(year)
        return format_month_filter(month, year)
    elif period.startswith("s"):
        season_number = int(period[1:])
        return format_season_filter(season_number)
    else:
        return str(period)


def format_league(league):
    if league == "base":
        return league
    elif league == "ck":
        return "Cities and Knights"

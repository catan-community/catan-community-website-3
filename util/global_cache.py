import flask_caching
import redis
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect

cache = flask_caching.Cache()
mongodb = None
csrf = CSRFProtect()

sql_db = SQLAlchemy()
migrate: Migrate
redis_db: redis.Redis

"""add referer column

Revision ID: 7c88dcf60530
Revises: 0dcd1d8ca5e1
Create Date: 2022-07-03 17:09:10.612215

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7c88dcf60530"
down_revision = "0dcd1d8ca5e1"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "user",
        sa.Column("referer", sa.String(length=127), nullable=False, server_default=""),
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("user", "referer")
    # ### end Alembic commands ###

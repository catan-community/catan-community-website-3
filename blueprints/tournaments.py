import flask
from models.tournament import Tournament, TournamentPlayer

tournaments = flask.Blueprint("tournaments", __name__, url_prefix="/tournaments")


@tournaments.route("/new", methods=["GET", "HEAD", "POST"])
def new_tournament():
    if flask.request.method == "POST":
        pass

    return flask.render_template("tournament_create.html", {})


@tournaments.route("/<int:tournament_id>")
def view_tournamnet(tournament_id):
    pass


@tournaments.route("/<int:tournament_id>/edit")
def edit_tournament(tournament_id):
    pass


@tournaments.route(
    "/<int:tournament_id>/update/<int:round>/<int:player>/<int:score>/<int:win>"
)
def update_score(tournament_id, round, player, score, win):
    pass


@tournaments.route("/<int:tournament_id>/reshuffle/<int:round>")
def update_round(tournament_id, round):
    pass


@tournaments.route("/<int:tournament_id>/set_public/<int:round>")
def set_public(tournament_id, round):
    pass

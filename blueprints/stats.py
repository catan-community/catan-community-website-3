import datetime

import flask

import models.user as user
import util.global_cache as global_cache

stats = flask.Blueprint(name="stats", import_name="stats", url_prefix="/stats")


def compute_stats_for_category(db_table):
    weekly_stats = [
        (i["_id"]["year"], i["_id"]["week"], i["count"])
        for i in db_table.aggregate(
            [
                {
                    "$group": {
                        "_id": {
                            "week": {
                                "$isoWeek": {
                                    "$dateFromParts": {
                                        "year": {"$year": "$_id"},
                                        "month": {"$month": "$_id"},
                                        "day": {"$dayOfMonth": "$_id"},
                                    }
                                }
                            },
                            "year": {
                                "$isoWeekYear": {
                                    "$dateFromParts": {
                                        "year": {"$year": "$_id"},
                                        "month": {"$month": "$_id"},
                                        "day": {"$dayOfMonth": "$_id"},
                                    }
                                }
                            },
                        },
                        "count": {"$sum": 1},
                    }
                }
            ]
        )
    ]

    weekly_stats.sort()

    stat_positions = [
        (
            (i[0] - 2021) * 53 + i[1],
            i[2],
            i,
            datetime.datetime.fromisocalendar(i[0], i[1], 1),
        )
        for i in weekly_stats
    ]
    return stat_positions


@stats.route("/weekly_stats")
def get_stats():
    database = global_cache.mongodb
    game_stats = compute_stats_for_category(database["matches"])
    player_stats = compute_stats_for_category(database["users"])

    return flask.render_template(
        "stats.html", game_stats=game_stats, player_stats=player_stats
    )


@stats.route("/new-users/<sort>/<int:page>")
@stats.route("/new-users")
def newly_registered_users(sort="updated", page=0):
    sort_option = {
        "pk": user.User.user_id.desc(),
        "updated": user.User.registration_update_date.desc(),
        "created": user.User.registration_date.desc(),
    }.get(sort)

    users = user.User.query.order_by(sort_option).paginate(page, 35, False)

    return flask.render_template("new_users.html", users=users, sort=sort)

import csv
import io
import traceback

import flask
import flask_sqlalchemy
import requests
from flask import Blueprint
from flask.helpers import flash, make_response
from sqlalchemy import exc, inspect
from wtforms import (
    BooleanField,
    Form,
    RadioField,
    StringField,
    ValidationError,
    validators,
)

import util.global_cache as global_cache
from models.user import User

reg = Blueprint("reg", __name__, url_prefix="/registration")


class RegistrationForm(Form):
    server = RadioField(choices=["yes", "no"])
    colonist = StringField(
        validators=[validators.Length(min=3, max=12), validators.InputRequired()]
    )
    country = StringField(
        validators=[validators.Length(min=3, max=80), validators.Optional()]
    )
    email = StringField(
        validators=[
            validators.Length(min=5, max=120),
            validators.Email(),
            validators.Optional(),
        ]
    )
    use_discord = RadioField(choices=["please-help", "no-help"])
    assistance = StringField(
        validators=[validators.Length(min=0, max=500), validators.Optional()]
    )
    ck = BooleanField()
    ck_5_6 = BooleanField()
    sf = BooleanField()
    sf_5_6 = BooleanField()
    cksf = BooleanField()
    cksf_5_6 = BooleanField()
    base_5_6 = BooleanField()
    base_7_8 = BooleanField()

    def validate_server(form, field):
        if field.data != "yes":
            raise ValidationError("You must join the discord to participate")


@reg.before_app_first_request
def setup_db():
    import app

    global_cache.sql_db.init_app(app.app)
    global_cache.sql_db.create_all()


@reg.route("/pre_register")
def pre_register():
    if flask.session.get("register_info"):
        id = int(flask.session["register_info"]["id"])
        if not is_registered(id):
            flask.session["registration_key"] = id
            return flask.redirect(flask.url_for("reg.register", user_id=id))
    return flask.redirect("/")


@reg.route("/register/<int:user_id>", methods=["GET", "POST"])
def register(user_id):
    if flask.session.get("registration_key") == user_id:
        form = RegistrationForm(flask.request.form)
        if (
            flask.request.method == "POST"
            and form.validate()
            and not is_registered(user_id)
        ):
            (res,) = global_cache.sql_db.session.query(
                User.query.filter(User.user_id == user_id).exists()
            ).first()
            if res:
                return update_registration(user_id)

            user = User(
                user_id=user_id,
                username=flask.session["register_info"]["username"]
                + "#"
                + flask.session["register_info"]["discriminator"],
            )
            global_cache.sql_db.session.add(user)
            try:
                user.update_auxiliary_fields(form)
            except exc.SQLAlchemyError:
                traceback.print_exc()
                print("form.errors=", form.errors)

                form.colonist.errors.append(
                    "Something went wrong submitting your registation. This usually means your colonist username or"
                    " email is not unique. Change them and if"
                    " the problem persists ask in the discord"
                )
                print(form.errors)
            else:
                send_webhook_message(user)
                flash("Registration Complete!")
                flask.session["user_info"] = flask.session["register_info"]
                flask.session.pop("register_info")
                flask.session.pop("registration_key")
                return flask.redirect("/registration/view")

        return flask.render_template(
            "register.html",
            form=form,
            discord_username=flask.session["register_info"]["username"]
            + "#"
            + flask.session["register_info"]["discriminator"],
        )
    return flask.abort(403)


def send_webhook_message(user: User):
    requests.post(
        flask.current_app.config.get("DISCORD_WEBHOOK_URL"),
        json={
            "content": f"""\
User <@{user.user_id}> ({user.username}) just registered on the website.
```diff
+ Colonist:                {user.colonist}
+ Country:                 {user.country}
+ Needs help with discord: {user.use_discord}
+ Special Accomodations:   {str(user.assistance)[:100]}
+ Referer:                 {user.referer}
```
See <https://catancommunity.org/stats/new-users> for mode details
        """,
            "username": user.username,
        },
    )


def is_registered(internal_user_id):
    return User.query.get(internal_user_id) is not None


@reg.route("/export_users")
def export_users():
    from app import is_admin

    if not is_admin():
        return flask.redirect("/403")
    data = io.StringIO()
    writer = csv.writer(data)
    users = User.query.all()
    column_names = [col.key for col in inspect(User).attrs]
    writer.writerow(column_names)
    for user_obj in users:
        writer.writerow([getattr(user_obj, col) for col in column_names])
    print(flask.request.path)
    response = make_response(data.getvalue())
    data.close()
    cd = "attachment; filename=AllUsers.csv"
    response.headers["Content-Disposition"] = cd
    response.mimetype = "text/csv"
    return response


@reg.route("/pre_update")
def pre_update_registration():
    if flask.session.get("user_info"):
        id = int(flask.session["user_info"]["id"])
        if is_registered(id):
            flask.session["update_key"] = id
            return flask.redirect(f"update/{id}")
    return flask.redirect("/")


@reg.route("/update/<int:user_id>", methods=["GET", "POST"])
def update_registration(user_id):
    if flask.session.get("update_key") == user_id:
        user = User.query.filter(User.user_id == user_id).first()

        if flask.request.method == "POST":
            form = RegistrationForm(flask.request.form)
        else:
            form = RegistrationForm(obj=user)
            form.server.data = "yes"

        if flask.request.method == "POST" and form.validate():
            user.update_auxiliary_fields(form)
            flask.session.pop("update_key")
            return flask.redirect("/registration/view/success")
        return flask.render_template(
            "register.html", form=form, discord_username=user.username
        )
    return flask.redirect("/")


@reg.route("/view", defaults={"success": None})
@reg.route("/view/<success>")
def view_registration(success):
    user_info = flask.session.get("user_info")
    if user_info:
        id = user_info["id"]
        if is_registered(id):
            user = User.query.get(id)
            updated = False
            if success == "success":
                updated = True
            return flask.render_template(
                "update_registration.html", user=user, view_only=True, success=updated
            )
    return flask.redirect("/")


@reg.route("/api/colonist_name", methods=["POST"])
@global_cache.csrf.exempt
def get_colonist_names():
    # float('nan') is guaranteed to not be equal to anything
    if flask.request.headers.get(
        "X-Authorization", float("nan")
    ) != flask.current_app.config.get("AUTHENTICATION_KEY"):
        return flask.abort(403)

    print("getting data")
    data = flask.request.json
    assert isinstance(data, list)
    for i in data:
        assert isinstance(i, int)

    registrations = User.query.filter(User.user_id.in_(data))
    colonist_names = {
        i.user_id: {
            "colonist_name": i.colonist,
            "expansions": {"c&k": i.ck, "seafarers": i.sf, "c&k+seafarers": i.cksf},
        }
        for i in registrations
    }

    print("\n".join(i.statement for i in flask_sqlalchemy.get_debug_queries()))

    return flask.jsonify(colonist_names)

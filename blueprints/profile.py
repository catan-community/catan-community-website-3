import flask

import util.global_cache as global_cache
import util.leaderboard_util as leaderboard_util
from models.user import User

profile = flask.Blueprint(name="profile", import_name="profile", url_prefix="/profile")


class PlayerProfile(global_cache.sql_db.Model):
    id = global_cache.sql_db.Column(global_cache.sql_db.Integer, primary_key=True)
    discord_id = global_cache.sql_db.Column(
        global_cache.sql_db.String, index=True, unique=True
    )
    content = global_cache.sql_db.Column(global_cache.sql_db.LargeBinary)


# @global_cache.cache.memoize(args_to_ignore=['database'], timeout=60 * 60 * 24)
def get_bffs(database, player_id):
    return [
        {
            "name": player["player"][0]["display_name"],
            "games": player["count"],
            "id": int(player["player"][0]["discord_username"][2:-1]),
        }
        for player in database["matches"].aggregate(
            [
                {"$match": {"players": f"<@{player_id}>"}},
                {"$project": {"players": 1}},
                {"$unwind": "$players"},
                {"$group": {"_id": {"player": "$players"}, "count": {"$sum": 1}}},
                {"$sort": {"count": -1}},
                {"$skip": 1},
                {"$limit": 10},
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "_id.player",
                        "foreignField": "discord_username",
                        "as": "player",
                    }
                },
            ]
        )
    ]


@profile.route("/<int:player_id>")
def view_profile(player_id):
    from app import is_admin

    player = global_cache.mongodb["users"].find_one(
        {"discord_username": f"<@{player_id}>"}
    )
    if player is None:
        return flask.render_template("fake_profile.html", player_id=player_id), 404

    rank = {
        league: tuple(
            leaderboard_util.get_user_rank(
                global_cache.mongodb,
                player["_id"],
                leaderboard_util.get_filter(i, league=league),
            )
            for i in (
                "month",
                "season",
                "all",
            )
        )
        for league in ("base", "ck")
    }

    profile_content = PlayerProfile.query.filter_by(discord_id=player_id).first()
    if profile_content is not None:
        profile_content = profile_content.content.decode("utf-8")

    if str(player_id) == flask.session.get("user_info", {}).get("id"):
        is_you = True
    else:
        is_you = False

    tournaments = global_cache.mongodb["user_lp"].find(
        {"type": "tourney", "user": player["_id"]}
    )

    if is_admin():
        user_model = User.query.filter_by(user_id=player_id).first()
        if user_model is not None:
            user = user_model.__dict__
        else:
            user = None
    else:
        user = None

    return flask.render_template(
        "profile.html",
        player=player,
        rank=rank,
        profile_content=profile_content,
        is_you=is_you,
        tournaments=tournaments,
        bffs=get_bffs(global_cache.mongodb, player_id),
        registation=user,
    )


@profile.route("/edit", methods=["GET", "POST"])
def edit_profile():
    discord_id = flask.session.get("user_info", {}).get("id")
    if discord_id is None:
        flask.abort(403)

    mongodb_user = (
        global_cache.mongodb["users"].find_one({"discord_username": f"<@{discord_id}>"})
        or {}
    )
    is_donator = mongodb_user.get("is_donator") or mongodb_user.get("is_admin")
    if is_donator:
        max_length = 1200
    else:
        max_length = 300

    user_profile = PlayerProfile.query.filter_by(discord_id=discord_id).first()
    if flask.request.method == "POST":
        if user_profile is None:
            user_profile = PlayerProfile(discord_id=str(discord_id), content=b"")
            global_cache.sql_db.session.add(user_profile)

        if len(flask.request.form["profile_content"]) > max_length:
            content = flask.request.form["profile_content"][:max_length]
        else:
            content = flask.request.form["profile_content"]

        user_profile.content = content.encode("utf-8")
        global_cache.sql_db.session.commit()

    return flask.render_template(
        "edit_profile.html",
        content=(user_profile and user_profile.content.decode("utf-8")) or "",
        max_length=max_length,
        player_id=int(discord_id),
    )

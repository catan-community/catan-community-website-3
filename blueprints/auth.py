import os

import flask
import requests_oauthlib
from flask import Blueprint

from blueprints.registration import is_registered

auth = Blueprint("auth", __name__, url_prefix="/auth")

oauth_token_url = "https://discordapp.com/api/oauth2/token"
oauth_authorize_url = "https://discordapp.com/api/oauth2/authorize"
oauth_api_base = "https://discordapp.com/api"

print(f"{os.environ.get('FLASK_ENV')=}")
if os.environ.get("FLASK_ENV") == "development":
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    protocol = "http://"
    print("enable insecure transport")
else:
    protocol = "https://"


@auth.route("/pre_login")
def pre_login():
    return '<a href="/auth/login">Login</a>'


@auth.route("/login")
def login():
    oauth = requests_oauthlib.OAuth2Session(
        flask.current_app.config["OAUTH2"]["client_id"],
        redirect_uri=protocol
        + flask.request.host
        + flask.url_for("auth.oauth_callback"),
        scope=flask.current_app.config["OAUTH2"]["scope"],
    )
    login_url, state = oauth.authorization_url(oauth_authorize_url)
    flask.session["oauth_state"] = state
    return flask.redirect(login_url)


@auth.route("/oauth_callback")
def oauth_callback():
    discord_session = requests_oauthlib.OAuth2Session(
        flask.current_app.config["OAUTH2"]["client_id"],
        redirect_uri=protocol
        + flask.request.host
        + flask.url_for("auth.oauth_callback"),
        state=flask.session["oauth_state"],
        scope=flask.current_app.config["OAUTH2"]["scope"],
    )
    token = discord_session.fetch_token(
        oauth_token_url,
        client_secret=flask.current_app.config["OAUTH2"]["client_secret"],
        authorization_response=flask.request.url,
    )
    flask.session["discord_token"] = token
    data = discord_session.get(oauth_api_base + "/users/@me").json()

    if not is_registered(data["id"]):
        flask.session["register_info"] = data
        return flask.redirect("/registration/pre_register")

    flask.session["user_info"] = data

    return flask.redirect("/")


@auth.route("/logout")
def logout():
    flask.session.pop("user_info")
    return flask.redirect("/")

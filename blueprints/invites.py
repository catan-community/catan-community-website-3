import datetime
import json
import random

import flask

import util.global_cache
import util.invites as invites

invite_manager = flask.Blueprint("invite_manager", "invites", url_prefix="/invites")


def register_invite():
    if "discord_invite" not in flask.session:
        flask.session["discord_invite"] = random.choice(invites.invites)

    if "https://" in str(flask.request.user_agent) or "http://" in str(
        flask.request.user_agent
    ):
        # Don't track bots
        return
    if "referrer" not in flask.session or flask.session["referrer"] is None:
        flask.session["referrer"] = flask.request.referrer
    if "first_page" not in flask.session:
        flask.session["first_page"] = flask.request.url
    flask.session["last_page"] = flask.request.url
    if "first_visit_time" not in flask.session:
        flask.session["first_visit_time"] = datetime.datetime.utcnow().isoformat()
    flask.session["last_visit_time"] = datetime.datetime.utcnow().isoformat()

    util.global_cache.redis_db.set(
        "invite_" + str(flask.session["discord_invite"]),
        json.dumps(
            {
                "referrer": flask.session["referrer"],
                "first_page": flask.session["first_page"],
                "last_page": flask.session["last_page"],
                "first_time": flask.session["first_visit_time"],
                "last_time": flask.session["last_visit_time"],
            }
        ).encode("utf-8"),
    )


@invite_manager.route("/all_invites")
def get_all_invites():
    return flask.jsonify(invites.invites)


@invite_manager.route("/get_referrer/<invite>")
def get_invite_referer(invite):
    data = util.global_cache.redis_db.get(f"invite_{invite}").decode("utf-8")
    if data is None:
        return flask.make_response(flask.jsonify(), 404)
    else:
        try:
            data = json.loads(data)
        except json.JSONDecodeError:
            data = {"referrer": data}
        return flask.jsonify(data)
